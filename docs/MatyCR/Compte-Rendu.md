---
title: Compte-rendu
lang: en
author: Maty Sarr
date: \today
pagesize: A4
toc: true
toc-depth: 5
fontsize: 12pt
documentclass: scrreprt
documentoptions: twoside
numbersections: true
colorlinks: blue
mainfont: Times New Roman
sansfont: Arial Black
monofont: Andale Mono
monofontoptions: 0.8
---

# Compte-Rendu
## Installation de nodejs
### Installation sur Windows

Pour windows, on peut télécharger nodejs par internet. 
Pour accéder à nodejs, on peut utiliser nodejs directement après l'avoir installé, ou passer par le **cmd**.

Pour connaître la version de nodejs qu'on a, on tape la commande:

> **node -v**

Nous avons installlé la version 16 de nodejs sur Windows.

On crée notre site qu'on va nommer projetbangui avec docusaurus, avec la commande:

> **npx create-docusaurus@latest projetbangui classic**

![](W1.PNG)

Ensuite on se déplace sur le dossier du site:

> **cd projetbangui**

Pour lancer le serveur, on exécute la commande suivante:

> **npm start**

Une fois le serveur lancé, nous pouvons accéder au site en mettant dans le navigateur:

> **localhost:3000**

![](W22.PNG)

### Installation sur Ubuntu
Sur Ubuntu, on installe d'abord le paquet curl:

> **# apt-get install curl**

![](./nodeU/1.PNG)

Après, on installe nodejs 18 à partir de nodesource:

> **# curl -sL https://deb.nodesource.com/setup_18.x | sudo bash -**

![](./nodeU/2.PNG)

On passe à l'installation de nodejs proprement dite:

> **# apt-get install -y nodejs**

![](./nodeU/3.PNG)

On vérifie que la version 18 de nodejs est bien installée:

> **node -v**

![](./nodeU/4.PNG)

On crée notre site nommé projetbangui à partir de docusaurus:

> **npx create-docusaurus@latest projetbangui classic**

![](./nodeU/5.PNG)

Nous avions premièrement installé Nodejs 18 sur Ubuntu 18, mais on a constaté que cette version n'est pas compatible avec celle de notre machine Ubuntu. Nous avons désinstallé Nodejs 18, et avons mis à jour notre système avec les commandes suivantes:

> **# apt dist-upgrade**
> **# do-release-upgrade**

On redémarre ensuite notre machine. Nous avons maintenant Ubunutu *20.04*.

### Présentation de Markdown:

La **Markdown** est un langage de balisage très facile à utiliser. Il a été créé en 2004 et permet d'écrire du texte pour le web et le convertir en HTML. Il repose sur un format de fichier textuel, similaire à HTML, mais avec une syntaxe plus légère. Il est la syntaxe de base sur docusaurus.

### Présentation de docusaurus:
**Docusaurus** permet de concevoir, déployer, et maintenir facilement des sites documentaires pour des projets open source. Il peut être customisé ou étendu selon nos besoins. Le support et la gestion des langues y sont intégrés, et tout ce qui y est produit comme contenu est rédigé avec la syntaxe markdown avant d'être généré sous la forme de fichiers HTML statiques.
En d'autres termes, **Docusaurus** est un générateur de site statique qui convertit des fichiers **Markdown** en un site web de documentation. Il est propulsé par React, qui est une bibliothèque javascript utilisée pour construire des composants d'interface utilisateur réutilisables, composables.

### Création d'un site avec docusaurus:
Pour créer un site à partir de docusaurus, on tape la commande suivante:

> **npx create-docusaurus@latest projetbangui classic**

Le nom de notre site ainsi créé est projetbangui.
Après avoir créé le site, on se dépalce dans le dossier du site avec la commande:

> **cd projetbangui**

Après s'être déplacé dans le dossier, on met en marche le serveur avec la commande:

> **npm start**:

![](./Docusaurus%20Ubuntu/Localhost.PNG)

Le serveur est en marche. Pour accéder au site, on met au niveau du navigateur:

> **localhost:3000**

![](./Docusaurus%20Ubuntu/Site1.PNG)

Le serveur peut aussi s'ouvrir automatiquement.

On voit ainsi la page principale du site.
Dans le dossier du site qu'on a créé avec docusaurus, on verra un fichier docs. Dans ce fichier, nous allons créer les fichiers de notre compte-rendu dans le dossier MatyCR que nous avons créé.
Le dossier MatyCR contient le dossier d'extension .json où nous allons mettre la position et le titre du site que nous allons créé.

![](./Docusaurus%20Ubuntu/json.PNG)

Dans le fichier markdown, nous allons faire le compte-rendu:

![](./Docusaurus%20Ubuntu/CR%201.PNG)
![](./Docusaurus%20Ubuntu/CR%204.PNG)

Une fois le serveur mis en marche, chaque compilation est notée, il nous renvoie aussi les erreurs. 

![](./Docusaurus%20Ubuntu/Compil%20Ubuntu.PNG)

Consultons le résultat dans le site:

![](./Docusaurus%20Ubuntu/R%201.PNG)
![](./Docusaurus%20Ubuntu/R%202.PNG)

Nous pouvons personnaliser notre site dans le fichier **docusaurus.config.js**, Nous y avons quelques modifications:

![](./Docusaurus%20Ubuntu/Monsite.PNG)

### Présentation de Pandoc:

Pandoc est un outil en ligne de commande qui permet de convertir du texte brut en différents types de fichiers bien formatés.

### Installation:

Installons **Pandoc** directement depuis GitHub:

![](./Pandoc/1.PNG)

Ensuite on installe le paquet au format .deb:

![](./Pandoc/2.PNG)

Après avoir installé **Pandoc**, on installe les prérequis:

![](./Pandoc/3.PNG)

![](./Pandoc/4.PNG)

pour vérifier la version de pandoc installé, on fait:

> **pandoc -v**

Maintenant Nous pouvons convertir un fichier **markdown** en un fichier **pdf**, on choisit de le faire avec le compte-rendu qu'on a fait. On se déplace d'abord dans le répertoire contenant le fichier **markdown**:

![](./Pandoc/transform.PNG)

Avec la commande **ls**, on voit que le fichier pdf est bien présent dans le répertoire. Donc la conversion a réussi.
On ouvre le fichier pdf pour vérifier:

![](./Pandoc/CR.PNG)

Le fichier pdf a bien été créé.

### KaTeX:
KaTeX est une bibliothèque JavaScript multi-navigateur qui affiche la notation mathématique dans les navigateurs Web. Il met l'accent sur le fait d'être rapide et facile à utiliser. Il a été initialement développé par Khan Academy, et est devenu l'un des cinq meilleurs projets sur GitHub en septembre 2014. KaTeX est basé sur TeX de Donald Knuth et rend ses calculs de manière synchrone. Il peut facilement être intégré aux ressources d'un site. Il produit la même sortie quelque soit le navigateur.

#### Quelques façons d'écrire avec KaTeX dans un fichier markdown:

![](./Formules%20katex/K1.PNG)
![](./Formules%20katex/K2.PNG)
![](./Formules%20katex/K3.PNG)

### Installation:
Installons KaTeX à partir de GitHub:

![](./Docusaurus%20Ubuntu/Katex%20installation.PNG)

On peut se déplacer dans son dossier en faisant: 

> **cd KaTeX**

Configurons KaTeX dans le fichier de configuration **docusaurus.config.js** pour sa prise en compte dans le site:

![](./Formules%20katex/conf%20katex%201.PNG)
![](./Formules%20katex/conf%20v.PNG)
![](./Formules%20katex/conf%20v2.PNG)

Testons la formule suivante:

$$
I = \int_0^{2\pi} \sin(x)\,dx
$$

### Présentation de GitLab:
 GitLab est un système de gestion de versions partagées. C'est une plateforme de développement collaborative open source. Elle couvre l'ensemble des étapes du DevOps. Se basant sur les fonctionnalités du logiciel Git, elle permet de piloter des dépôts de code source et de gérer leurs différentes versions.

#### Installation:
 Pour installer Gitlab, on exécute d'abord la commande suivante:
 
 > **apt update && apt upgrade**

 On installe ensuite les dépendances:

 ![](./Gitlab/G1.PNG)

 On télécharge le script GitLab APT avec la commande curl:

 ![](./Gitlab/G2.PNG)

 On synchronise le nouveau référentiel:

![](./Gitlab/G3.PNG)

 On installe GitLab:

 ![](./Gitlab/G4.PNG)

 Après installation, on obtient la sortie suivante:

 ![](./Gitlab/G5.PNG)

 ### Version statique du site:
Générons la version statique du site:

![](./Docusaurus%20Ubuntu/npm%20run%20buildStatic.PNG)

On voit que le dossier **build** du site est créé, il contient les fichiers statiques. 
A la racine du projet, on crée le fichier d'extension **.yml**. Ce fichier va lancer un **npm run build** pour vérifier que le build fonctionne bien, mais aussi lancer le build et générer un artefact avec la version statique de notre docusaurus lors d'un push.
Nous avons nommé ce document **.gitlab-ci.yml**, Nous l'avons édité de la manière suivante pour ajouter un pipeline pour le déploiement:

![](./Docusaurus%20Ubuntu/yml.PNG)

### Ajout d'un moteur de recherche:
Pour se faire, installons la dépendance docusaurus-search-local:

![](./Docusaurus%20Ubuntu/npm%20install.PNG)
![](./Docusaurus%20Ubuntu/search.PNG)

Ajoutons la recherche dans le fichier docusaurus.config.js:

![](./Docusaurus%20Ubuntu/conf%20for%20search.PNG)

Maintenant testons:

![](./Docusaurus%20Ubuntu/npmrunbuildForSearch.PNG)
![](./Docusaurus%20Ubuntu/searchMade.PNG)

### Openfire:
Openfire est un serveur de collaboration en temps réel (RTC) sous licence Open Source Apache. Il est également connu sous le nom de serveur XMPP. Il est utilisé pour créer des applications de chat au niveau élémentaire. Openfire XMPP est extrêmement facile à configurer et à entretenir. Openfire peut utiliser sa propre base de données ou se connecter sur un SGDB externe comme Microsoft SQL, Oracle ou MySQL.
Les utilisateurs peuvent aussi être internes au système, ou relié à un serveur LDAP comme l’Active Dirtectory (AD).

#### Caractéristiques d'Openfire:
-Prise en charge de l'intégration LDAP et avec d'autres serveurs de messagerie instantanée externes.
-Prise en charge de la base de données intégrée (PostgreSQL, Oracle, MySQL) pour plus de détails sur les utilisateurs et le stockage des messages.
-Intégration du client de messagerie instantanée Spark
-Il peut également prendre en charge SSL et TSL pour la sécurité.
-Openfire contient également un gestionnaire pour le gestionnaire de configuration de l'interface graphique Web.
-Une variété de plug-ins étendent ses fonctionnalités.

### Installation:

Avant de l'installer, on met d'abord à jour la liste des paquets:

> **# apt update**

Ensuite on télécharge le package .deb du serveur de chat Openfire XMPP sur notre système:

![](./OpenFire/1.PNG)

Installons le package Openfire téléchargé:

![](./OpenFire/2.PNG)

Activons le service Openfire puis vérifions son état:

![](./OpenFire/3.PNG)

On voit que Openfire est bien active.

#### Installation de MariaDB:
MariaDB est l'une des bases de données open source les plus populaires à côté de son créateur MySQL. Les créateurs originaux de MySQL ont développé MariaDB en réponse aux craintes que MySQL deviendrait soudainement un service payant en raison de son acquisition par Oracle en 2010. Avec son histoire de faire des tactiques similaires, les développeurs derrière MariaDB ont promis de le garder open source et exempt de craintes.

Installons MariaDB sur notre machine Ubuntu *20.04*:

![](./OpenFire/install%20mariadb.PNG)

Nous avons installé la version *10.3* de MariaDB, on constate que la connexion à notre base de donnée ne peut se faire. Nous avons édité le fichier /etc/mysql/mysql.conf.d/mysqld.cnf en remplaçant l'adresse IP de **bind-addr** par *0.0.0.0*.
En supposant que la version *10.3* de MariaDB n'est pas compatible avec notre machine Ubuntu *20.04*, désinstallons MariaDB et installons sa plus récente version:

> **# apt purge mariadb-server**

Installons MariDB *10.7* sur notre machine Ubuntu *20.04*:
 Mettons d'abord à jour le gestionnaire de paquets:

 > **# apt update && upgrade -y**

 Installons les dépendances nécessaires à l'installation:

 ![](./mariadb/1.PNG)

 Importons le référentiel MariaDB 10.7 depuis MariaDB. Tout d'abord, on importe la clé GPG à l'aide de la commande suivante:

 ![](./mariadb/2.PNG)

 Ensuite, importons le référentiel MariaDB *10.7*:

 ![](./mariadb/3.PNG)

 Maintenant que la clé et le référentiel sont importés, mettons à jour la liste du gestionnaire de packages:

 > **# apt update**

 Importons le référentiel MariaDB 10.7 à l'aide du script Bash officiel:

 ![](./mariadb/4.PNG)

 Actualisons la liste de référentiels APT:

 > **# apt update**

Nous pouvons maintenant installer les packages client et serveur:

![](./mariadb/5.PNG)

Vérifions la version et létat du service:

![](./mariadb/6.PNG)

Sécurisons MariaDB 10.7 avec un script de sécurité:

![](./mariadb/8.PNG)
![](./mariadb/9.PNG)
![](./mariadb/10.PNG)

Telles sont les instructions que nous avons suivi:
-Définition du mot de passe pour racine comptes.
-Suppression des comptes root accessibles depuis l'extérieur de l'hôte local.
-Suppression des comptes d'utilisateurs anonymes.
-Suppression de la base de données de test, accessible par défaut aux utilisateurs anonymes.

Connectons-nous maintenant à notre serveur de base de données. Créons un nom de base de données en tant que "openfire", attribuons un mot de passe et des privilèges.

![](./mariadb/11.PNG)

Puisque nous avons déjà installé notre serveur de base de données, revenons maintenant à Openfire. Au niveau du navigateur, utilisons **localhost:9090** pour faire la configuration web d'Openfire. **9090** est le numéro de port d'Openfire.
On choisit la langue:
 
![](./OpenFire/conf%20lang.PNG)

Configurons les paramètres du serveur en ajoutant le nom de domaine, le nom d'hôte du serveur, le port de la console d'administration et un port de console sécurisé:

![](./OpenFire/serveur.PNG)

Ensuite, choisissons la façon dont nous souhaitons nous connecter à la base de données Openfire:

![](./OpenFire/BD.PNG)

Configurons notre profil:

![](./OpenFire/profil.PNG)

Ajoutons l'adresse e-mail et le mot de passe de l'administrateur:

![](./OpenFire/account%20mdp.PNG)
![](./OpenFire/install%20termin%C3%A9%C3%A9.PNG)

La configuration est terminée, maintenant connectons-nous à l'interface Web Openfire en saisissant admin comme login et le mot de passe de l'administrateur:

![](./OpenFire/Connexion%20au%20serveurD.PNG)

Nous pouvons maintenant accéder à Openfire.
Dans l'onglet Plugins, si on clique sur Téléchargement de plugin, on ne trouve pas le plugin **Pade** sur la liste. Donc nous allons télécharger le plugin dans le site officiel et l'exporter dans Openfire.

![](./OpenFire/addPade.PNG)

Nous avons constaté que la version *1.7.0* du plugin **Pade**, utilisé pour les vidéo-conférences, ne marche pas avec la version *4.5.2* d'Openfire que nous avons installé. Donc nous allons désinstaller la version d'Openfire installée et installer la version *4.7.1.* Faisons d'abord un purge de la version installée:

> **# apt purge openfire**

Maintenant, nous allons télécharger la version .deb d'Openfire sur le site officiel.

![](./OpenFire/telOp.PNG)

Après téléchargement, on désarchive le fichier téléchargé avec la commande:

> **# tar -xzf openfire_4_7_1.tar.gz**

on se déplace dans le répertoire contenant openfire pour le démarrer:

![](./OpenFire/openfire%204.7.PNG)

Maintenant configurons notre nouveau serveur Openfire. On met au niveau du navigateur:

> **localhost:9090**

Nous avons suivi les mêmes étapes que précédemment, c'est seulement l'utilisation de la base de donnée qui est modifiée. Nous avons utilisé pour cette fois, une connexion standard à la base de donnée.
Vérifions d'abord les informations sur notre base de données MariaDB en nous connectant avec le compte openfire que nous avons créé:

![](./OpenFire/mariadb1.PNG)
![](./OpenFire/mariadb2.PNG)

Nous avons renseigné les champs de connexion à notre base de données sur MariaDB:

![](./OpenFire/47Op2.PNG)

Une fois connecté à Openfire, on télécharge et ajoute le plugin **Pade** aux plugins disponibles:

![](./OpenFire/padeV.PNG)
![](./OpenFire/pad%C3%A9Ajouter.PNG)

**Pade** a bien été ajouté.

Tels sont les paramètres de la conférence que nous avons modifié en mettant notre adresse IP et le numéro de port:

![](./OpenFire/Open/P1.PNG)
![](./OpenFire/Open/P2.PNG)
![](./OpenFire/Open/P3.PNG)

Tels sont les paramètres du salon:

![](./OpenFire/Open/1.PNG)
![](./OpenFire/Open/2.PNG)
![](./OpenFire/Open/4.PNG)
![](./OpenFire/Open/5.PNG)
![](./OpenFire/Open/6.PNG)
![](./OpenFire/Open/7.PNG)

Après installation de **Pade** et configuration, nous pouvons maintenant accéder à une conférence.

![](./OpenFire/Open/PADE%20MEETING.PNG)

Nous donnons un nom à notre conférence, ensuite on se connecte avec notre compte:

![](./OpenFire/Open/name.PNG)

Nous sommes maintenant dans la conférence.

![](./OpenFire/Open/meet.PNG)

Créons le compte utilisateur ALpha, à qui nous allons donner tous les privilèges, et le simple utilisateur Kiné:

![](./OpenFire/Open/users.PNG)

Connexion avec l'utilisateur Alpha:

![](./OpenFire/Open/Connexion%20with%20alpha.PNG)

[Vous pouvez lire notre compte-rendu en version pdf](./Compte-Rendu.pdf)
